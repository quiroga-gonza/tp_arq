# Instalación de programas necesarios

Para poder utilizar las herramientas de este repositorio es necesario tener instaldo:

- Git
- Docker

A continuación se presentan los pasos y links de descarga necesarios para poder instalar los programas mencionados. Pueden utilizarse estos programas en Windows 10 Pro o en cualquier distribución de GNU/Linux.

# Programas para Windows

## Descargar Git para Windows

Descagar e instalar desde este sitio:

`https://git-scm.com/download/win`

## Docker para Windows

Descagar e instalar desde este sitio:

`https://hub.docker.com/editions/community/docker-ce-desktop-windows`


# Programas para Linux

## Git para Linux (Distribuciones basadas en Debian, por ejemplo Ubuntu)

En una terminal, ejecutar el siguiente comando:

`sudo apt-get install git`

## Instalar Docker

Se recomienda instalar la versión Docker CE. En el siguiente link hay un tutorial paso a paso para hacerlo:

`https://www.digitalocean.com/community/tutorials/como-instalar-y-usar-docker-en-ubuntu-18-04-1-es`

Existen múltiples tutoriales en internet sobre como instalar docker. Si el tutorial anterior no sirve, pruebe buscando otras alternativas.