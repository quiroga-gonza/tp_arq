import cocotb
from cocotb.clock import Clock
from cocotb.triggers import Timer, RisingEdge, FallingEdge, Edge, Event
from cocotb.result import TestFailure, TestError, ReturnValue, SimFailure

CLK_PERIOD = 10

@cocotb.coroutine
def Reset(dut):
    dut.RST_i <=  0
    yield Timer(CLK_PERIOD * 1)
    dut.RST_i  <= 1
    yield Timer(CLK_PERIOD * 10)
    dut.RST_i  <= 0

@cocotb.test()
def test(dut):
    """
    Description:
        Test RISC-V Simple Implementation
    """
    cocotb.fork(Clock(dut.CLK_i, CLK_PERIOD).start())
    
    yield Reset(dut)
    
    #--Variables auxiliares
    #opcode = " "    
    #--	
    #yield Timer(CLK_PERIOD * 64)
    print("\n\n")
    print("-------------------------------")
    print("Procesador Quiroga - Villarruel")
    print("-------------------------------")
    for i in range(64):
        yield RisingEdge(dut.CLK_i)
        opcode = ""
        tipo = ""
        print("Instrucción: \n",dut.Instruction.value)
    #-------Esto es para ver cómo entra la instrucción, opcional los prints
        #print("7 bits:", end=""),
        #for j in range(0,7): #opcode
        print(dut.Instruction.value[0:6],end=""),

        print("\t",end="")
            #print("\n")
            #print("5 bits:", end=""), #rd o inmediato
        #for j in range(7,12):
        print(dut.Instruction.value[7:11],end=""),
            
        print("\t",end="")
            #print("\n")
            #print("5 bits:", end=""), #funct3
        #for j in range(12,17):
        print(dut.Instruction.value[12:16],end=""),
            
        print("\t",end="")
            #print("\n")
            #print("3 bits:", end=""), #rs1
        #for j in range(17,20):
        print(dut.Instruction.value[17:19],end=""),
            
        print("\t",end="")
            #print("\n")
            #print("5 bits:", end=""), #rs2 o inmediato
        #for j in range(20,25):
        print(dut.Instruction.value[20:24],end=""),
            
        print("\t",end="")
            #print("\n")
            #print("7 bits:", end=""), #funct7 o inmediato
        #for j in range(25,32):
        opcode += str(dut.Instruction.value[25:31])
        print(dut.Instruction.value[25:31],end=""),
        print("\n")
            
        #-------------------------------------------------------------------
        print("Program Counter: ",dut.PCOUT.value.integer)
        print("\n")
        print("opcode:", opcode)
        if(opcode == "0110011"):
            print("Tipo de instruccion: R")
            tipo = "R"
        elif(opcode == "0100011"):
            print("Tipo de instruccion: S")
            tipo = "S"
        elif(opcode == "1100111"):
            print("Tipo de instruccion: SB")
            tipo = "SB"
        elif(opcode == "0000011" or opcode == "0010011" or opcode == "1100111"):
            print("Tipo de instruccion: I",end=""),
            if(opcode == "0000011"):
            	print(" - Load")
            elif(opcode == "1100111"):
            	print("- JAL")
            else:
            	print("\n")
            tipo = "I"
        else:
            print("Formato no válido")
        print("\n")
        print("Inmediato extendido en 64 bits:",dut.ImmGenOUT.value)
        print("\n")
        
        print("Banco de registros:")
        print("\t Primer registro fuente: " + str( dut.Instruction.value[12:16]) + " - X" + str(dut.Instruction.value[12:16].integer))
        
        if(tipo == "I"):
            if(str(dut.Instruction.value[0:5]) == "010000" or str(dut.Instruction.value[0:5]) == "000000"):
                print("\t Registro fuente inmediato: " + str( dut.Instruction.value[6:11]) + " - " + str(dut.Instruction.value[6:11].integer))
            else:
                print("\t Registro fuente inmediato: " + str( dut.Instruction.value[0:11]) + " - " + str(dut.Instruction.value[0:11].integer))
            print("\t Registro destino: " + str( dut.Instruction.value[20:24]) + " - X" + str(dut.Instruction.value[20:24].integer))
        
        elif(tipo == "S"):
            print("\t Segundo registro fuente: " + str( dut.Instruction.value[7:11]) + " - X" + str(dut.Instruction.value[7:11].integer))
            inmed = str(dut.Instruction.value[0:6]) + str(dut.Instruction.value[20:24])
            inmed_int = str(dut.Instruction.value[0:6].integer) + str(dut.Instruction.value[20:24].integer)
            print("\t Inmediato: "+ inmed +" - "+ inmed_int)
        
        elif(tipo == "SB"):
            print("\t Registro fuente 2: " + str( dut.Instruction.value[7:11]) + " - X" + str(dut.Instruction.value[7:11].integer))
            # por como está formado el inmediato, no acepta valores que sean impares del número que uno piensa, es decir, los 
	    # va a recortar, xq no toma el bit 0 del número deseado
            # o eso entiendo de la tabla de la teoría
            inmed = str(dut.Instruction.value[0]) + str(dut.Instruction.value[24])  + str(dut.Instruction.value[1:6]) + str(dut.Instruction.value[20:23])
            inmed_int = str(dut.Instruction.value[0].integer) + str(dut.Instruction.value[24].integer)  + str(dut.Instruction.value[1:6].integer) + str(dut.Instruction.value[20:23].integer)
            print("\t Inmediato: "+ inmed +" - "+ inmed_int)
        
        else: 
            print("\t Segundo Registro fuente: " + str( dut.Instruction.value[7:11]) + " - X" + str(dut.Instruction.value[7:11].integer))
            print("\t Registro destino: " + str( dut.Instruction.value[20:24]) + " - X" + str(dut.Instruction.value[20:24].integer))

        print("\t Escribir en registro:", dut.RegWrite.value)
        print("\t Salida del primer registro:", dut.ReadDATA1toALU.value)
        print("\t Salida del segundo registro:", dut.ReadDATA2toALU.value)
        print("\t Dato que se escribe en el registro:", dut.MUXtoREGISTERS.value)
        print("\n")
        
        print("ALU:")
        print("\t Entrada1 (del banco de registro):", dut.ReadDATA1toALU.value)
        print("\t Entrada2 (del mux):", dut.MuxtoALU.value)
        print("\t Salida:",dut.ALUresult.value)
        print("\t Zero:",dut.ZERO.value)
        print("\t Se va a realizar: ", dut.ALUOp.value,end=""),
            
        if(dut.ALUOp.value.integer == 0):
            print(" -> AND\n")
        elif(dut.ALUOp.value.integer == 1):
            print(" -> OR\n")
        elif(dut.ALUOp.value.integer == 2):
            print(" -> ADD\n")
        elif(dut.ALUOp.value.integer == 5):
            print(" -> XOR\n")
        elif(dut.ALUOp.value.integer == 6):
            print(" -> SUB\n")
        elif(dut.ALUOp.value.integer == 8):
            print(" -> SLL\n")
        elif(dut.ALUOp.value.integer == 9):
            print(" -> SRL\n")
        elif(dut.ALUOp.value.integer == 10):
            print(" -> SLA\n")
        elif(dut.ALUOp.value.integer == 11):
            print(" -> SRA\n")
        elif(dut.ALUOp.value.integer == 13):
            print(" -> BEQ\n")
        elif(dut.ALUOp.value.integer == 12):
            print(" -> BNE\n")
        elif(dut.ALUOp.value.integer == 3):
            print(" -> BLT\n")
        elif(dut.ALUOp.value.integer == 7):
            print(" -> BGE\n")
        elif(dut.ALUOp.value.integer == 15):
            print(" -> BLTU\n")
        elif(dut.ALUOp.value.integer == 14):
            print(" -> BGEU\n")
        else:
            print(" Funcion desconocida \n")
            
        print("Memoria:")
        print("\t ALUresult:",dut.ALUresult.value)
        print("\t ReadDATA2toALU:",dut.ReadDATA2toALU.value)
        print("\t DataMEMtoMux:",dut.DataMEMtoMux.value)
        print("\t MemWrite:",dut.MemWrite.value)
        print("\t MemRead:",dut.MemRead.value)
        print("\n")
        
        
        
        print("Unidad de control:")
        print("\t Branch:",dut.Branch.value)
        print("\t MemRead:",dut.MemRead.value)
        print("\t MemtoReg:",dut.MemtoReg.value)
        print("\t ALUOp:",dut.ALUOp.value)
        print("\t MemWrite:",dut.MemWrite.value)
        print("\t ALUSrc:",dut.ALUSrc.value)
        print("\t RegWrite:",dut.RegWrite.value)
        print("\t UC_MUX:",dut.UC_MUX.value)
        print("\t Mux2reg:",dut.MUXtoREGISTERS.value)
        print("-------------------------------------")
        print("\n\n")
