# Tp5


## Materia
Arquitectura de Computadoras año 2022
## Autores
Quiroga Gonzalo, Villarruel Enzo

## Profesores
Airabella Migue Andres, Astri Eith Andrada

## Description
El proyecto consiste en armar un procesador Risc-v, mediante la implementación de codigos previsto por la catedra.

## Ejercicio 1
Se clono el repositorio en el ordenar para poder empezar a trabajar con los codigos. Utilizando el codigo:

#git clone https://gitlab.com/unsl-up/risc-v 

Una vez con el repositorio en el ordenador, se procedio a realizar los siguientes ejercicios.

## Ejercicio 2
Los IP cores se encuentran en la carpeta SCR, mientras que el directorio de Test está en test_phyton.

##Ejercicio 3
Se ingresó al directorio del Banco de Registros, y se ejecuó la simulación, utilizando el dockershell. En el mismo se encontró que como se inicializan los reguistros en 1, como en 0 según el caso. El Testeo corrio con normalidad.

## Ejercicio 4
Una vez que se ubicaron los archivos de IP cores y los test, se procedio a revisar el codigo de la ALU.

Se agregaron instrucciones como son BEQ, BNE, BLT, BGE, BLTU, BGUE. Con estas se logró todo el set de instrucciones basico de una ALU. Posteriormente se realizo el testbech con sus modificaciones.

## Ejercicio 5
Se procedio a realizar un estructura de directorios, para poder copiar los IP Cores con sus respectivos test, para luego poder integrar todos los IP-Cores y podes formar el microprocesador deseado.


