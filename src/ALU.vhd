library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity alu is

generic (N : integer :=64);
port (
      A_i      : in  std_logic_vector(N - 1 downto 0);
      B_i      : in  std_logic_vector(N - 1 downto 0);
      OUTPUT_o : out std_logic_vector(N - 1 downto 0);
      ALUop_i  : in  std_logic_vector(3 downto 0);
      Zero_o   : out std_logic
      );
end entity;

architecture alu_arch of alu is
----------------------------
-- Operaciones Originales --
----------------------------

-- Logicas --
 
constant AND_IN : std_logic_vector(3 downto 0) := "0000";  -- 0 
constant OR_IN  : std_logic_vector(3 downto 0) := "0001";  -- 1
constant ADD    : std_logic_vector(3 downto 0) := "0010";  -- 2
constant XOR_IN : std_logic_vector(3 downto 0) := "0101";  -- 5
constant SUB    : std_logic_vector(3 downto 0) := "0110";  -- 6 

-- Saltos --

constant SLL_IN : std_logic_vector(3 downto 0) := "1000"; -- 8
constant SRL_IN : std_logic_vector(3 downto 0) := "1001"; -- 9
constant SLA_IN : std_logic_vector(3 downto 0) := "1010"; -- 10
constant SRA_IN : std_logic_vector(3 downto 0) := "1011"; -- 11

------------------------
-- Operaciones Nuevas --
------------------------

-- Comparaciones -- 

constant BEQ   : std_logic_vector(3 downto 0) := "1101"; -- 13 Igualdad
constant BNE   : std_logic_vector(3 downto 0) := "1100"; -- 12 Desigualdad
constant BLT   : std_logic_vector(3 downto 0) := "0011"; -- 3 Menor
constant BGE   : std_logic_vector(3 downto 0) := "0111"; -- 7 Mayor Igual
constant BLTU  : std_logic_vector(3 downto 0) := "1111"; -- 15  Menor Sin Signo
constant BGEU  : std_logic_vector(3 downto 0) := "1110"; -- 14  Mayor Sin Signo




signal output_s : std_logic_vector(N-1 downto 0);
signal zero: std_logic;
begin

OUTPUT_o <= output_s;
Zero_o <= zero;

--Zero_o <= '1' when output_s = std_logic_vector(to_unsigned(0,N)) else '0'; 

process(A_i, B_i, ALUop_i)

variable aux1 : std_logic;
variable aux2 : std_logic_vector(N-1 downto 0);

begin
    case ALUop_i is
        when ADD =>
            zero <= '0';
            output_s <= std_logic_vector((unsigned(A_i) + unsigned(B_i)));
        when SUB =>
            zero <= '0';
            output_s <= std_logic_vector((unsigned(A_i) - unsigned(B_i)));
        when AND_IN =>
            zero <= '0';
            output_s <= A_i and B_i;
        when OR_IN =>
            zero <= '0';
            output_s <= A_i or B_i;
        when XOR_IN =>
            zero <= '0';
            output_s <= A_i xor B_i;
        when SLL_IN =>
            zero <= '0';
            output_s <= std_logic_vector(shift_left(unsigned(A_i), to_integer(unsigned(B_i))));
        when SRL_IN =>
            zero <= '0';
            output_s <= std_logic_vector(shift_right(unsigned(A_i), to_integer(unsigned(B_i))));
        when SLA_IN =>
       	    zero <= '0';
            output_s <= std_logic_vector(shift_left(signed(A_i), to_integer(unsigned(B_i))));
        when SRA_IN =>
            zero <= '0';
            output_s <= std_logic_vector(shift_right(signed(A_i), to_integer(unsigned(B_i))));
        
        -- Funciones Nuevas --
	-- Dejar estas notas para luego
	-- Primero hago todos ceros, para asegurarme que el menos significativo NO haya quedado con "mugre"
        -- Además, sirve para que cuando no cumpla la condición, ya toda la salida está en cero... Nice
        
        when BEQ =>
            output_s <= (others =>'0');		
            if(A_i = B_i) then
            	zero <= '1';
            	output_s(0) <= '1'; 
            else
            	zero <= '0';
            end if;	
        when BNE =>
            output_s <= (others =>'0');
            if(A_i /= B_i) then
            	zero <= '1';
            	output_s(0) <= '1'; 
            else
            	zero <= '0';	
            end if;
        when BLT =>
            output_s <= (others =>'0');
            if(A_i < B_i) then
            	zero <= '1';
            	output_s(0) <= '1'; 
            else
            	zero <= '0';	
            end if;
        when BGE =>
            output_s <= (others =>'0');
            if(A_i >= B_i) then
            	zero <= '1';
            	output_s(0) <= '1'; 
            else 
            	zero <= '0';	
            end if;
        when BLTU =>
            output_s <= (others =>'0');
            if(unsigned(A_i) < unsigned(B_i)) then
            	zero <= '1';
            	output_s(0) <= '1'; 
            else
            	zero <= '0';	
            end if;
        when BGEU =>
            output_s <= (others =>'0');
            if(unsigned(A_i) >= unsigned(B_i)) then
            	zero <= '1';
            	output_s(0) <= '1'; 
            else
            	zero <= '0';	
            end if;
        when others => output_s <= (others => '0');  -- Esto se coloca para evitar casos no definidos   
    end case;
        
    end process;
end alu_arch;
